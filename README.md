**Note**: Fork from [thomasleveil/doco-maltrail](https://github.com/thomasleveil/doco-maltrail)

[Maltrail](https://github.com/stamparm/maltrail) is a malicious traffic detection system, utilizing publicly available (black)lists containing malicious and/or generally suspicious trails, along with static trails compiled from various AV reports and custom user defined lists, where trail can be anything from domain name (e.g. zvpprsensinaix.com for Banjori malware), URL (e.g. http://109.162.38.120/harsh02.exe for known malicious executable), IP address (e.g. 185.130.5.231 for known attacker) or HTTP User-Agent header value (e.g. sqlmap for automatic SQL injection and database takeover tool). Also, it uses (optional) advanced heuristic mechanisms that can help in discovery of unknown threats (e.g. new malware).


## CI/CD

Every month, on the 25, the image is updated by a Gitlab CI/CD job and pushed into the container registry of this repo.


## Docker-compose

Integration of the maltrail sensor and server in a docker-compose configuration: 
```yml
version: '3.8'
services:
    server:
        image: registry.gitlab.com/lobelia/maltrail:latest
        volumes:
            - ./maltrail.conf:/maltrail/maltrail.conf:ro
            - ./logs:/var/log/maltrail
        ports:
            - 8338:8338
            - 127.0.0.1:8337:8337/udp
        command: python server.py
    
    sensor:
        image: registry.gitlab.com/lobelia/maltrail:latest
        privileged: true
        network_mode: host
        volumes:
            - ./maltrail.conf:/maltrail/maltrail.conf:ro
        command: python sensor.py
```

## Notes

The maltrail configuration can be downladed at : https://raw.githubusercontent.com/thomasleveil/doco-maltrail/master/maltrail.conf

Then visit http://127.0.0.1:8338/ and sign in with `admin`:`changeme!`. 
